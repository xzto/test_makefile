#!/bin/sh

# This script is used to rebuild everything automatically if you happen to change CFLAGS or something.
# "$1": Name for the file (usually BUILD_DIR/.cached_vars.txt)
# stdin: The concatenated variables you want to cache (CC, CFLAGS, etc)

set -e

# echo " -- make_cached_vars.sh: start" >&2

dep_file="$1"
new_hash="$(sha256sum -)"
old_hash=""
if [ -r "$dep_file" ]; then
    old_hash="$(cat "$dep_file" || true)"
else
    mkdir -p "$(dirname "$dep_file")"
fi

exit_rebuild() {
    echo rebuild
    exit 1
}

exit_ok() {
    exit 0
}

if [ -n "$old_hash" ]; then
    if [ "${new_hash}" != "${old_hash}" ]; then
        echo "$new_hash" > "$dep_file"
        exit_rebuild
    else
        exit_ok
    fi
else
    echo "$new_hash" > "$dep_file"
    exit_ok
fi
