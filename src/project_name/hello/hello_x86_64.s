.extern puts

.global hello
// void hello(void);
hello:
  push %rdi
  lea hello_world_text(%rip), %rdi
  call puts
  pop %rdi
  ret

.global say
// void say(char const *str);
//   %rdi: str
say:
  call puts
  ret

hello_world_text: .asciz "Hello, world"
