TOP := $(shell pwd)
BUILD_DIR ?= build
tmp := $(shell mkdir -p $(BUILD_DIR))


# A C compiler for the host system.
HOSTCC ?= cc

# A C compiler for the target system
CC ?= cc
# An assembler for the target
AS = $(CC)
# A linker for the target
LD = $(CC)

mycflags-opt := -O0 -ggdb3
# mycflags-opt := -Os -g0
mycflags-opt += -flto
mycflags-opt += -ffunction-sections -fdata-sections
mycflags := -std=gnu11
mycflags += -Wall -Wextra -Werror -Werror=return-type -Wconversion \
	-Wimplicit-fallthrough -Wuninitialized -Wignored-attributes \
	-Winit-self -Wshadow -Wundef -Wfloat-conversion -Wsign-conversion \
	-Wno-unused-function \
	-Wtype-limits -Wwrite-strings -Werror=implicit-function-declaration \
	-Wunused-result -Wnarrowing -Wmissing-field-initializers
mycflags += -Isrc/

mycflags += $(mycflags-opt)
mycflags += $(CFLAGS)

myasflags := $(mycflags)

myldflags := $(mycflags) -Wl,--fatal-warnings -Wl,--sort-section=name -Wl,--gc-sections
myldflags += $(LDFLAGS)


# `infiles-y`: All .c, .s, .S files
infiles-y :=

# src/Makefile will add to `infiles-y`
include src/Makefile

objs := $(addprefix $(BUILD_DIR)/, $(addsuffix .o, $(infiles-y)))
deps := $(shell find $(BUILD_DIR)/ -name '*.d')
-include $(deps)

ECHO_HOSTCC  := " hostcc  "
ECHO_CC      := " cc      "
ECHO_LD      := " ld      "
ECHO_AS      := " as      "
ECHO_CODEGEN := " codegen "

.SECONDARY: $(BUILD_DIR) $(objs)
.PHONY: all show clean clean_build_dir codegen clean_codegen hostcc clean_hostcc
.DEFAULT_GOAL := all

all: out Makefile


# Re build everything if CFLAGS or anything changed.
# I make a target called $(cached_vars_file) and everything will depend on it.
# When variables change, the target will be marked as PHONY (always make it)
# therefore all targets that depend on $(cached_vars_file) will also run.
cached_vars_file := $(BUILD_DIR)/.cached_vars.txt
$(cached_vars_file): ;

ifeq (rebuild,$(shell echo "$(mycflags)$(myldflags)$(myasflags)$(HOSTCC)$(CC)$(LD)$(AS)" | ./scripts/make_cached_vars.sh $(cached_vars_file)))
.PHONY: $(cached_vars_file)
endif



show:
	@echo CC = $(CC)
	@echo AS = $(AS)
	@echo LD = $(LD)
	@echo BUILD_DIR = $(BUILD_DIR)
	@echo "# "objs = $(objs)
	@echo "# "infiles = $(infiles-y)
	@echo cflags = $(mycflags)
	@#echo ldflags = $(myldflags)
	@#echo asflags = $(myasflags)

clean: clean_build_dir clean_codegen ;

clean_build_dir:
	rm -rf "$(BUILD_DIR)"

codegen: ;
clean_codegen: ;

hostcc: ;
clean_hostcc: ;


$(BUILD_DIR)/%:
	@echo Shit $@
	@echo This probably means the file was not found or there is no rule for this file.
	@false

$(BUILD_DIR)/%.d: ;

$(BUILD_DIR)/%.c.o: %.c Makefile $(cached_vars_file)
	@mkdir -p "$(@D)"
	@echo $(ECHO_CC) $<
	@$(CC) $(mycflags) -MMD -c $< -o $@

$(BUILD_DIR)/%.s.o: %.s Makefile $(cached_vars_file)
	@mkdir -p "$(@D)"
	@echo $(ECHO_AS) $<
	@$(AS) $(myasflags) -c $< -o $@

out: $(BUILD_DIR)/out
$(BUILD_DIR)/out: $(objs) Makefile $(cached_vars_file)
	@echo $(ECHO_LD) $(BUILD_DIR)/out
	@$(LD) $(myldflags) $(objs) -o $(BUILD_DIR)/out

