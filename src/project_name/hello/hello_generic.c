int puts(char const *);

void hello(void) {
  puts("Hello, world");
}

void say(char const *str) {
  puts(str);
}
